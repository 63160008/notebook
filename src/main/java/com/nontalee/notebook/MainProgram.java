/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nontalee.notebook;

/**
 *
 * @author nonta
 */
public class MainProgram {

    public static void main(String[] args) {
        Notebook n1 = new Notebook("9999", "i5-9300", 16, 1024);
        Notebook n2 = new Macbook("7777", "Mac os", 64, 1024);
        Acer acer = new Acer("1234", "Ryzen9", 32, 1024);
        System.out.println(n1.toString());
        System.out.println(n1.getName());
        System.out.println(n2.toString());
        System.out.println(n2.getName());
        System.out.println(acer.getUser());
        System.out.println(acer.getUser("Tokyo"));
    }
}
